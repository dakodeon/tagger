import sys
from pathlib import Path
# if we want info relevant to the file, like bitrate etc, we should also import the MP3 class
from mutagen.easyid3 import EasyID3 as id3

validtags = {
    "title": "TIT2",
    "alt-title": "TIT3",
    "composer": "TCOM",
    "singer": "TPE1",
    "musician": "TPE2",
    "lyricist": "TEXT",
    "TXXX:scale": "scale",
    "TXXX:rhythm": "rhythm",
    "TXXX:my-keys": "my-keys",
    "key": "TKEY",
    "year": "TDRL",
    "mood": "TMOO",
    "original-artist": "TOPE",
    "original-year": "TDOR"
    # "comments": "COMM"
             }

def as_string(l):
    full = ""
    for val in l:
        full += val + ", "
    full=full.removesuffix(", ")
    return full

def has_key(d, key):
    if key in d:
        return d[key]
    else:
        return ""

class song:
    """Song class"""
    def __init__(self, file):
        self.path = Path(file).resolve()
        self.name = self.path.stem
        self.tags = id3(self.path)
        # Register valid tags -- should do this outside init, but how?
        for key, val in validtags.items():
            if key.startswith("TXXX:"):
                self.tags.RegisterTXXXKey(val, val)
            else:
                self.tags.RegisterTextKey(key, val)
        # self.currentvals = self.read_current_vals()
        # read current values from the existing tags
        self.currentvals = {}
        for key in validtags.keys():
            key = key.removeprefix("TXXX:")
            if key in self.tags:
                self.currentvals[key] = as_string(self.tags[key])
            else:
                self.currentvals[key] = ""
        self.ttags = self.tags_as_text(self.tags)

    def tags_as_text(self, tags):
        txttags = {}
        for key in tags.keys():
            if key in validtags:
                txttags[key] = as_string(has_key[tags, key])
        return txttags

    # assign a current value
    def set_value(self, key, val):
        self.currentvals[key] = val

    # pass current values to the tags and save them
    def write_tags(self):
        for key, val in self.currentvals.items():
            if self.currentvals[key] == "":
                try:
                    del self.tags[key]
                except:
                    continue
            self.tags[key] = val
        self.tags.save(self.path)

    def purge_tags(self):
        self.tags.delete(self.path)
        self.tags.save(self.path)
        self.ttags = self.tags_as_text(self.tags)

    def rename(self):
        tags = self.ttags
        main_artist = ""
        if has_key(tags, "singer") == "" and has_key(tags, "musician") != "":
            main_artist = tags["musician"]
        else:
            main_artist = tags["singer"]
            
        if has_key(tags, "title") != "" and main_artist != "":
            if has_key(tags, "composer") == "" or tags["composer"] == main_artist:
                newname = main_artist
            else:
                newname = tags["composer"] + " (" + main_artist + ")"
            newname = newname + " - " + tags["title"]
            if has_key(tags, "alt-title") != "":
                newname = newname + " (" + tags["alt-title"] + ")"
            newpath = self.path.with_name(newname).with_suffix(".mp3")
            self.path.replace(newpath)
            self.path = newpath
            self.name = self.path.stem
        else:
            print("name could not be changed")
