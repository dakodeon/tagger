import tagger
import tagger_conf
import argparse
from pathlib import Path

valid_tags = list(tagger_conf.tag_sets.keys())

parser = argparse.ArgumentParser(description="ID3 tags manipulation utility -- by LukeBass", epilog="For more information, visit https://www.lukebass.xyz.")
parser.add_argument('-r', '--rename', help="Rename files according to a scheme based on the given tags.", action="store_true")
parser.add_argument('-t', '--tags', help="Which set of tags to use. Valid sets are defined in the config file.", choices=valid_tags)
parser.add_argument('FILES', help="Files or directories from which to read mp3 files.", nargs='*')

args = parser.parse_args()

# get the preferred tag set
tag_set = tagger_conf.tag_sets[args.tags]

# read mp3 files from given files and directories
def read_songs(files):
    songs = []
    
    for file in files:
        filename = Path(file).resolve()
        if filename.is_file() and filename.suffix == '.mp3':
            songs.append(filename)
        elif filename.is_dir():
            for f in filename.glob('*.mp3'):
                songs.append(f.resolve())

        if len(songs) == 0:
            # throw an error here and exit
            print("No mp3 files found!")
            exit(33)
        else:
            return songs


valid_files = read_songs(args.FILES)

print("Songs to work with:")
for song in valid_files:
    print(song)
            
print("\nTags to work with in the {} set:".format(args.tags))
for key, val in tag_set.items():
    print("{}: {}".format(key, val))
