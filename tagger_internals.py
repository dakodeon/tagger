import sys
from pathlib import Path
import mutagen.id3 as id3
from mutagen.mp3 import MP3 as mp3

class Song:
    """
    Stores info about the song, as well as methods to manipulate the tags and the filename.
    """
    def __init__(self, file):
        self.path = Path(file).resolve()
        self.name = self.path.stem
        self.mp3info = mp3(self.path).info
        self.id3tags = mp3(self.path).tags

    def tags_prettyprint(self):
        print(self.id3tags.pprint())
