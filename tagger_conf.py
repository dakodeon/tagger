import tomllib

with open("conf.toml", "rb") as cfg:
    options = tomllib.load(cfg)

tag_sets = options["tags"]
